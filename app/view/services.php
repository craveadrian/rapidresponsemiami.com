	<div id="content">
		<div class="row">
			<h1>Services</h1>
			<div class="services">
				<h2>Water Damage Removal and Restoration | Flood Damage Repair</h2>

				<p>Whether you've had an AC leak, pipe burst, shower leak, flood damage or any other water damage condition it should be addressed quickly. Treating any water damage condition will eliminate the development of dangerous bacteria such as mold.</p>

				<p>Rapid Response Restoration offers 24-hour emergency water removal and restoration services with a fast 30-minute response time.</p>

				<h3>Our water related specialties include:</h3>
				<ul>
					<li>Flood Damage Restoration</li>
					<li>Water Damage Clean up</li>
					<li>Water Extraction</li>
					<li>Water Damage Repair</li>
					<li>Water Damage Clean Up</li>
					<li>Flood Damage Repair</li>
					<li>Water Damage Restoration Service</li>
					<li>Flood Damage Service</li>
					<li>Flood Restoration Clean up</li>
				</ul>


				<h2>Fire and Smoke Damage Repair and Restoration</h2>

				<p>Whether it be from a kitchen fire or even a lightning strike, Fire damage can be devastating and cause emotional exhaustion, confusion and frustration to any home or business owner. Rapid Response Restoration Fire damage repair professionals are readily available 24 hours a day to respond within 30 minutes and restore your property from all of the smoke and fire damage.</p>

				<h2>MOLD CLEANUP REMEDIATION</h2>

				<p>Mold is capable of growing within 48-72 hours following a water leak. If you were not aware of a water leak in your home, the odds of mold being present is fairly high. Mold spreads pollutants and irritants in the air and can make the indoor air quality dangerous for you and your loved ones. Here at Rapid Response Restoration we are there for you with 24/7 emergency services. If you suspect you might have a mold problem call a professional, IICRC certified company, to schedule an inspection immediately.</p>

				<p>Once on site our technicians are able to assess your unique situation. Our technicians are able to provide you with an estimate of the cost based on the extent of the damages; we use Xactimate for our clients’ billing, which is the insurance industry standard software for emergency services. Once our contract is signed, our technicians are able to start building a containment to protect the remainder of your home while they work on the affected areas. We work for you and can deal directly with your insurance company on your behalf to get your situation handled in a professional and timely manner. We use state-of-the-art equipment, anti microbial’s and follow industry standard guidelines for Mold Remediation Miami so you can rest easy knowing professionals are in your home who will get the job done right.</p>
			</div>
		</div>
	</div>
